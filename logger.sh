#!/usr/bin/env bash

###############################################################################
# Provides simply log functions.
# See https://bitbucket.org/erikrj/bash-helpers/src/master/
#
# This file may be sources remotaly by doing
#
#   source <(curl -s "https://bitbucket.org/erikrj/bash-helpers/raw/master/logger.sh")
#
# This file may be downloaded remotely by doing
#
#   curl -sO "https://bitbucket.org/erikrj/bash-helpers/raw/master/logger.sh"
#
# Notes:
#
# By default this script will create shortnames for its functions.
#
# Example:
#   foo_logger_trace is also avaialble as trace
#   foo_logger_debug is also avaialble as debug
#   foo_logger_info is also avaialble as info
#   foo_logger_warn is also avaialble as warn
#   foo_logger_error is also avaialble as error
#   foo_logger_error is also avaialble as fatal
#
# To disable this functionality set the following before sourcing this file
#
#   FOO_LOGGER_DISABLE_ALIASES=1
#
# To instead override the aliases used, set the following before sourcing this
# file.
#
#   FOO_LOGGER_FUNCTION_ALIAS="myalias_"
#
#
# This script checks for the existence of a function
#
# This class depends on color.sh. If this script cannot fund color.sh to source
# it will download a copy. To disasble this behavior you can set one of two
# variables.
#
# FOO_DOWNLOAD_DEPENDENCIES=0
# FOO_LOGGER_DOWNLOAD_DEPENDENCIES=0
#
# The HTTP location to download the file may be overriden by setting
#
# FOO_LOGGER_DOWNLOAD_LOCATION="http://example.com/colors.sh"
#
###############################################################################

#------------------------------------------------------------------------------
# Source Dependencies
#------------------------------------------------------------------------------

if [[ -n "${FOO_DOWNLOAD_DEPENDENCIES+z}" ]]; then
  FOO_LOGGER_DOWNLOAD_DEPENDENCIES=${FOO_DOWNLOAD_DEPENDENCIES}
fi

if [[ -z "${FOO_LOGGER_DOWNLOAD_DEPENDENCIES+z}" ]]; then
  FOO_LOGGER_DOWNLOAD_DEPENDENCIES=1
fi

if [[ -z "${FOO_LOGGER_COLORS_DOWNLOAD_LOCATION+z}" ]]; then
  FOO_LOGGER_DOWNLOAD_LOCATION="https://bitbucket.org/erikrj/bash-helpers/raw/master/"
fi

if [[ -z "${FOO_LOGGER_DISPLAY_BASENAME+z}" ]]; then
  FOO_LOGGER_DISPLAY_BASENAME=1
fi

# shellcheck source=colors.sh
function foo_logger_source() {
  if [[ -f "${1}" ]]; then
    source "${1}"
  elif [[ -f $(pwd)/${1} ]]; then
    source "$(pwd)${1}"
  elif [[ "${FOO_LOGGER_DOWNLOAD_DEPENDENCIES}" == 1 ]]; then
    source <(curl -s "${FOO_LOGGER_DOWNLOAD_LOCATION}${1}")
  fi
}

[[ ! $(declare -f "foo_colors_reset") ]] && foo_logger_source "colors.sh"

#------------------------------------------------------------------------------
# Logging Functions
#------------------------------------------------------------------------------

declare -Ai FOO_LOGGER_LEVELS=(
  [TRACE]=5
  [DEBUG]=4
  [INFO]=3
  [WARN]=2
  [ERROR]=1
  [FATAL]=0
)

declare -A FOO_LOGGER_LEVELS_COLOR=(
  [TRACE]=$(foo_colors_magenta)
  [DEBUG]=$(foo_colors_blue)
  [INFO]=$(foo_colors_green)
  [WARN]=$(foo_colors_yellow)
  [ERROR]=$(foo_colors_red)
  [FATAL]=$(foo_colors_red)
)

if [[ -z "${FOO_LOGGER_LEVEL+z}" ]]; then
  FOO_LOGGER_LEVEL=${FOO_LOGGER_LEVELS["INFO"]}
fi

function foo_logger_print_level() {
  echo "${FOO_LOGGER_LEVEL}"
}

function _foo_logger_log() {
  local name && name=${1} && shift
  local basename && basename="[$(basename "${0}")] "
  [[ ${FOO_LOGGER_DISPLAY_BASENAME} == 0 ]] && basename=""
  local color && color=${FOO_LOGGER_LEVELS_COLOR[${name}]}
  if [[ ${FOO_LOGGER_LEVEL} -ge ${FOO_LOGGER_LEVELS[${name}]} ]]; then
    if [[ ${FOO_LOGGER_LEVELS[${name}]} -le 1 ]]; then
      1>&2 printf "${color}%-7s${basename}$(foo_colors_reset)${*}\n" "${name,,}:"
    else
      printf "${color}%-7s${basename}$(foo_colors_reset)${*}\n" "${name,,}:"
    fi
    [[ ${FOO_LOGGER_LEVELS[${name}]} != 0 ]] || exit 1
  fi
}

# The follwing will produce functions called
#   foo_logger_enable_<level>
#   foo_logger_<level>
for name in ${!FOO_LOGGER_LEVELS[*]}; do
  eval "function foo_logger_enable_${name,,}() { FOO_LOGGER_LEVEL=${FOO_LOGGER_LEVELS[${name}]}; }"
  eval "function foo_logger_${name,,}() { _foo_logger_log ${name} \${@}; }"
done

#------------------------------------------------------------------------------
# Allow functions to be aliased under a different prefixes
#------------------------------------------------------------------------------
if [[ -z "${FOO_LOGGER_FUNCTION_ALIAS+z}" ]]; then
  FOO_LOGGER_FUNCTION_ALIAS=""
fi

if [[ -z "${FOO_LOGGER_DISABLE_ALIASES+z}" ]]; then
  FOO_LOGGER_DISABLE_ALIASES=0
fi

if [[ "${FOO_LOGGER_DISABLE_ALIASES}" == 0 ]]; then
  for fn in $(compgen -A function); do
    eval "$(echo function "${FOO_LOGGER_FUNCTION_ALIAS}${fn//foo_logger_}"; declare -f "${fn}" | tail -n +2)"
  done
fi
