# TrueMark Linux Toolbox

This repository contains utilities useful for dealing with automating linux. 

## Installation

The following command will install the utilities into /usr/local/bin

```bash
bash <(curl -sL https://bitbucket.org/truemark/linux-toolbox/raw/master/install)
```

