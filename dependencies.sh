#!/usr/bin/env bash

function ubuntu() {
  export DEBIAN_FRONTEND=noninteractive
  apt-get -y update
  apt-get -y install nvme-cli
}

function redhat() {
  yum install -y nvme-cli
}

command -v apt-get &> /dev/null && ubuntu
command -v yum &> /dev/null && redhat
