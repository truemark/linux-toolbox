#!/usr/bin/env bash

###############################################################################
# Provides functionality to color and style output to the terminal
# See https://bitbucket.org/erikrj/bash-helpers/src/master/
#
# You can source this file remotely by doing
#
#   source <(curl -s "https://bitbucket.org/erikrj/bash-helpers/raw/master/colors.sh")
#
# Notes:
#
# By default this script will create shortnames for its functions.
#
# Example:
#   foo_colors_red is also avaialble as cred
#   foo_colors_bgblue is also available as cbgblue
#   foo_colors_reset is also available as creset
#
# To disable this functionality set the following before sourcing this file
#
#   FOO_COLORS_DISABLE_ALIASES=1
#
# To instead override the aliases used, set the following before sourcing this
# file.
#
#   FOO_COLORS_FUNCTION_ALIAS="myalias"
#
# Some helpful information regarding VT100 escape codes can be found at
# http://www.termsys.demon.co.uk/vtansi.htm
###############################################################################

declare -A FOO_COLORS=(
  [BLACK]="0"
  [RED]="1"
  [GREEN]="2"
  [YELLOW]="3"
  [BLUE]="4"
  [MAGENTA]="5"
  [CYAN]="6"
  [WHITE]="7"
)

#------------------------------------------------------------------------------
# V1T00 Colors
#------------------------------------------------------------------------------
FOO_COLORS_VT100_FG="\033[;3" # foreground color
FOO_COLORS_VT100_BG="\033[;4" # background color
FOO_COLORS_VT100_RESET="\033[0m";

declare -A FOO_VT100_STYLES=(
  [BOLD]="\033[1m"
  [DIM]="\033[2m"
  [UNDERLINE]="\033[4m"
)

declare -A FOO_VT100_COLORS=(
  [DEFAULT]="${FOO_COLORS_VT100_FG}9m"
)

declare -A FOO_VT100_BG_COLORS=(
  [DEFAULT]="${FOO_COLORS_VT100_BG}9m"
)

# Dynamiclaly add colors to FOO_VT100_COLORS
for color in ${!FOO_COLORS[*]}; do
  FOO_VT100_COLORS[${color}]="${FOO_COLORS_VT100_FG}${FOO_COLORS[${color}]}m"
  FOO_VT100_BG_COLORS[${color}]="${FOO_COLORS_VT100_BG}${FOO_COLORS[${color}]}m"
done

# The following will produce a function called foo_colors_<color> for each color
for name in ${!FOO_VT100_COLORS[*]}; do
  eval "function foo_colors_${name,,}() { echo -en \"${FOO_VT100_COLORS[${name}]}\"; }"
done

# The following will produce a function called foo_colors_<style> for each color
for name in ${!FOO_VT100_STYLES[*]}; do
  eval "function foo_colors_${name,,}() { echo -en \"${FOO_VT100_STYLES[${name}]}\"; }"
done

# The follwing will produce a function called foo_colors_bg_<color> for each background color
for name in ${!FOO_VT100_BG_COLORS[*]}; do
  eval "function foo_colors_bg${name,,}() { echo -en \"${FOO_VT100_BG_COLORS[${name}]}\"; }"
done

function foo_colors_reset() {
  echo -en "${FOO_COLORS_VT100_RESET}"
}

function foo_colors_print_styles() {
  for style in ${!FOO_VT100_STYLES[*]}; do
    echo -e "${FOO_VT100_STYLES[${style}]}${style}$(foo_colors_reset)"
    foo_colors_reset
  done
}

function foo_colors_print_colors() {
  for color in ${!FOO_VT100_COLORS[*]}; do
    echo -e "${FOO_VT100_COLORS[${color}]}${color}$(foo_colors_reset)"
  done
}

function foo_colors_print_bg_colors() {
  for color in ${!FOO_VT100_BG_COLORS[*]}; do
    echo -e "${FOO_VT100_BG_COLORS[${color}]}${color}$(foo_colors_reset)"
  done
}

function foo_colors_print_all() {
  printf "Background Colors\n"
  foo_colors_print_bg_colors
  printf "\nStyles\n"
  foo_colors_print_styles
  printf "\nForeground Colors\n"
  foo_colors_print_colors
}

function foo_colors_print() {
  for style in ${!FOO_VT100_STYLES[*]}; do
    for color in ${!FOO_VT100_COLORS[*]}; do
      echo -en "${FOO_VT100_STYLES[${style}]}${FOO_VT100_COLORS[${color}]}${style} ${color}$(foo_colors_reset)"
    done
  done
}

#------------------------------------------------------------------------------
# Allow functions to be aliased under a different prefixes
#------------------------------------------------------------------------------
if [[ -z "${FOO_COLORS_FUNCTION_ALIAS+z}" ]]; then
  FOO_COLORS_FUNCTION_ALIAS="c"
fi

if [[ -z "${FOO_COLORS_DISABLE_ALIASES+z}" ]]; then
  FOO_COLORS_DISABLE_ALIASES=0
fi

if [[ "${FOO_COLORS_DISABLE_ALIASES}" == 0 ]]; then
  for fn in $(compgen -A function); do
    eval "$(echo function "${FOO_COLORS_FUNCTION_ALIAS}${fn//foo_colors_}"; declare -f "${fn}" | tail -n +2)"
  done
fi
